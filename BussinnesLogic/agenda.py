from DataAccess.conexionMysql import Conexion
from DataAccess.conexionMongo import ConexionM



class Contacto:

    def __init__(self):
        self.conexion=Conexion()
        self.conexionM=ConexionM()
    
    def listarContacto(self):
        query="select * from contacto"
        result=self.conexion.ejecutar_query(query)
        filas=result.fetchall()
        print()
        for fila in filas:
            print(fila[1])

    def anadirContacto(self, nombre , celular , telefono , email , fechaNacimiento , direccion):
        query=f"insert into contacto (nombre, telefonoCelular, telefonoFijo, email, fechaNacimiento, direccion) VALUES ('{nombre}','{celular}', '{telefono}', '{email}', '{fechaNacimiento}','{direccion}')"
        resultado = self.conexion.ejecutar_query(query)
        print(resultado)

# METODOS DE MONGO

    def listarContactoMongo(self):
        for x in self.conexionM.mycol.find({},{'_id':0}):
            print(x)

    def anadirContactoMongo(self, nombre , celular , telefono , email , fechaNacimiento , direccion):
        mydict = { "nombre":nombre, "telefonoCelular":celular, "telefonoFijo":telefono, "email":email, "fechaNacimiento":fechaNacimiento, "direccion":direccion }
        x = self.conexionM.mycol.insert_one(mydict)
        print(x.inserted_id)

