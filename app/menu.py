from app.switch import OptionSwitcher

import app.custom as custom

# La clase debe ser con la primera letras en Mayusculas y en caso sea compuesta debe ser un camelCase
class MenuAgenda:

    optionSwitcher = ''

    # El metodo que se ejecuta al instanciar la clase.
    def __init__(self):
        self.optionSwitcher = OptionSwitcher()
        print('Metodo INIT de la clase MenuAgenda')

    # Metodo - Menu del programa
    def mostrar(self,optionBD):
        try:
            menu = [
                ['Agenda Personal'],
                ['----------------'],
                ['1. Añadir Contacto'],
                ['2. Lista de contactos'],
                ['3. Buscar contacto'],
                ['4. Editar contacto'],
                ['5. Eliminar contacto'],
                ['9. Cerrar agenda']
            ]
            bd=""
            if(optionBD==1):
                bd="MySql"
            else:
                bd="MongoDB"

            print("\n\n\n\n ESTAS TRABAJANDO CON LA BBDD  "+bd)
            print('----------------')
            for x in range(len(menu)):
                print(menu[x][0])

            print()
            option = int(input("Introdusca la opción deseada: "))
            optionbd=optionBD
            self.validarOption(option,optionbd)

        except ValueError:
            print()
            print('ERROR : Solo esta permitido el ingreso de numero....!!!')
            print()
            self.mostrar(optionbd)

    # Metodo
    # Para validar que la opcion ingresada por el usuario se encuenta
    # entre en el rango permitido.
    def validarOption(self, option,optionbd):
        optionBaseDatos=optionbd
        if option == 9:
            print("Saliendo de la agenda ...")
            exit()

        if option >= 1  and option <= 5:
            custom.imprimir('RESULTADO')
            self.optionSwitcher.main(option,optionBaseDatos)
            print()
            self.mostrar(optionBaseDatos)
        else:
            # Mensaje de opcion incorrecta y volver a mostrar el menu
            print()
            print("Uhs opcion incorrecta, intente nuevamente")
            print()
            self.mostrar(optionBaseDatos)