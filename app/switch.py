import json
from BussinessLogic.agenda import Contacto

class OptionSwitcher:

    def __init__(self):
        # Instanciamos la clase AgendaTelefonica
        self.contacto = Contacto()

    def main(self, argument, optionBD):
        optionbd=optionBD
        method_name = 'option_' + str(argument)
        method = getattr(self, method_name, lambda: "Opcion Invalida")
        return method(optionBD)
        
    #añadir contacto
    def option_1(self,objBD):
                
        nombre = input('Nombre : ')
        celular = input('Celular : ')
        telefono = input('Telefono : ')
        email = input('Correo Electronico : ')
        fechaNacimiento = input('Fecha Nacimiento : ')
        direccion = input(' Dirección : ')

        if(objBD==1):
            self.contacto.anadirContacto(nombre = nombre, celular = celular, telefono = telefono, email = email, fechaNacimiento = fechaNacimiento, direccion=direccion)
        else:
            self.contacto.anadirContactoMongo(nombre = nombre, celular = celular, telefono = telefono, email = email, fechaNacimiento = fechaNacimiento, direccion=direccion)
                   
    #listar contacto
    def option_2(self, objBD):
        if(objBD==1):
            self.contacto.listarContacto()
        else:
            self.contacto.listarContactoMongo()
        
    
    #buscar contacto
    def option_3(self, objBD):
        dni = input('Ingrese el DNI del contacto a buscar : ')
        contacto = self.contacto.buscarContacto(dni)
        if contacto is None:
            print ('Uhs el contacto buscado no existe')
        else:
            print(json.dumps(contacto, indent=2))
    
    #editar contacto
    def option_4(self, objBD):
        print('No hace nada la opcion numero 4')
    
    #eliminar contacto
    def option_5(self, objBD):
        dni = input('Ingrese el DNI del contacto a buscar : ')
        resultado = self.contacto.eliminarContacto(dni)
        if resultado is None:
            print ('Uhs el contacto que intenta eliminar no existe')
        else:
            print ('El contacto ha sido eliminado correctamente.....!!!! ')
